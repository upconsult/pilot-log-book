# Flite - Digital Pilot Logbook for Air Balloon Pilots

Welcome to Flite, the ultimate digital logbook designed specifically for air balloon pilots. Flite serves as a comprehensive platform, offering a seamless and intuitive way for pilots to log their flights, track essential information, and determine their 'current' status in maintaining up-to-date flight records.

<hr/>

| Branch      |                                                       Pipeline                                                       |                                         Coverage                                         |
| ----------- | :------------------------------------------------------------------------------------------------------------------: | :--------------------------------------------------------------------------------------: |
| Main        |      ![pipeline main](https://gitlab.com/upconsult/pilot-log-book/badges/main/pipeline.svg?ignore_skipped=true)      |  ![coverage main](https://gitlab.com/upconsult/pilot-log-book/badges/main/coverage.svg)  |
| Development | ![pipeline develop](https://gitlab.com/upconsult/pilot-log-book/badges/development/pipeline.svg?ignore_skipped=true) | ![coverage](https://gitlab.com/upconsult/pilot-log-book/badges/development/coverage.svg) |

## Table of contents

- [Flite - Digital Pilot Logbook for Air Balloon Pilots](#flite---digital-pilot-logbook-for-air-balloon-pilots)
  - [Table of contents](#table-of-contents)
  - [Visuals](#visuals)
  - [Environments](#environments)
  - [Support](#support)
  - [Contributing](#contributing)
    - [Requirements](#requirements)
    - [Branching strategy](#branching-strategy)
    - [Scripts](#scripts)
    - [Testing](#testing)
      - [Unit tests](#unit-tests)
      - [End-to-end tests](#end-to-end-tests)
    - [Linting](#linting)
    - [Git hooks](#git-hooks)
    - [Configs](#configs)
  - [Authors and acknowledgment](#authors-and-acknowledgment)
  - [License](#license)

## Visuals

<img src="docs/videos/flite.gif" alt="flite" width="200"/>
<img src="docs/images/add-flight.png" alt="flite" width="200"/>
<img src="docs/images/logbook.png" alt="flite" width="200"/>

## Environments

The app has two environments:

- [Main]() (not available yet)
- [Development](https://test.flite.quickrelease.aero/)

## Support

For assistance or queries, please access our issue ticket tracker on the PXL Jira instance. Although the primary issue tracker is on the school's Jira, this will probably eventually transition entirely to GitLab.

<!-- ## Roadmap -->

<!-- TODO: Add roadmap -->

## Contributing

### Requirements

- Node.js
- Access to the Firebase project

### Branching strategy

- See our workflow [here](docs/workflow.md).
- See how to branch [here](docs/branching.md).
- See how to commit [here](docs/commits.md).
  - Also see how our commit messages are linted [here](docs/commits.md#commit-linter).

### Scripts

We have a lot of scripts to make our lives easier. You can find them in the [package.json](package.json) file. Here are some of the most important ones:

- `npm run start` - Starts the app in development mode.
- `npm run build` - Builds the app for production to the `dist` folder.
  <hr/>
- `npm run test` - Launches the test runner in the interactive watch mode.
- `npm run test-headless` - Launches the test runner in headless mode. Useful for CI/CD.
- `npm run cypress` - Launches the Cypress test runner in interactive mode.
- `npm run e2e` - Launches the Cypress test runner in headless mode. Useful for CI/CD.
  <hr/>
- `npm run lint` - Lints the codebase.
- `npm run lint:fix` - Lints the codebase and fixes the errors.
- `npm run performance` - Runs the performance tests.
  <hr/>
- `npm run prepare` - Runs the `prepare` script for the `husky` package. This is used to set up the Git hooks. This is not necessary if you run `npm install`.

### Testing

#### Unit tests

We use [Jasmine](https://jasmine.github.io/) and [Karma](https://karma-runner.github.io/latest/index.html) for unit testing. The unit tests are located in the [src/app](src/app/) folder and have the `.spec.ts` extension.

The Karma configuration is located in the [karma.conf.js](karma.conf.js) file. Here we also specify how the reports are generated for the ci/cd pipeline.

#### End-to-end tests

We use [Cypress](https://www.cypress.io/) for end-to-end testing. The end-to-end tests are located in the [cypress/e2e](cypress/e2e/) folder and have the `.spec.cy.ts` extension.

The Cypress configuration is located in the [cypress.config.ts](cypress.config.ts) file. Important here is how defines the base url for the tests.

### Linting

We use [ESLint](https://eslint.org/) with [Prettier](https://prettier.io/) to lint our code. The linting rules are configured in the [.eslintrc.json](.eslintrc.json) file and the [.prettierrc.json](.prettierrc.json).

### Git hooks

We use [husky](https://typicode.github.io/husky/#/) to set up Git hooks. The Git hooks are configured in the `.husky` folder.

For pre-commit hooks, we use [lint-staged](https://github.com/lint-staged/lint-staged) to only lint the staged files. This is done to prevent unnecessary linting of the entire codebase.

For pre-push hooks, we ensure that the **unit** tests pass before pushing to the remote repository.

Git hooks are automatically set up when you run `npm install`. If you want to manually set up the Git hooks, you can run `npm run prepare`.

Git hooks can also be bypassed by adding the `--no-verify` flag to your Git command. For example:

```powershell
git commit -m "chore: make commit" --no-verify
```

### Configs

If you want to know what the rest of the configs do, you can find a more detailed explanation [here](docs/configs.md).

## Authors and acknowledgment

| Name               | Role                               |
| ------------------ | ---------------------------------- |
| Wim Maet           | Product Owner                      |
| Bart Clijsnet      | Team Coach                         |
| Omar Abdulhamid    | Software Manager                   |
| Matthias Willems   | Developer                          |
| Jan Haegdorens     | Developer                          |
| Marc Vanheusden    | Developer                          |
| Vincent Vandebosch | Developer                          |
| Rasmus Leseberg    | System and Network Manager         |
| Nele Custers       | Technical Development Coach        |
| Maarten Sourbron   | Technical System and Network Coach |
| Tom Cool           | Technical System and Network Coach |
| Nathalie Fuchs     | Technical Coach                    |

## License

The project license still needs to be determined.
