import { IPilot } from '../models/pilot';
import { AuthService } from './auth.service';
import { TestBed } from '@angular/core/testing';
import { AngularFireAuth } from '@angular/fire/compat/auth/';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { of } from 'rxjs';

describe('AuthService', () => {
  let service: AuthService;
  let mockAngularFireAuth: {
    signInWithEmailAndPassword: jasmine.Spy<jasmine.Func>;
    createUserWithEmailAndPassword: jasmine.Spy<jasmine.Func>;
    sendPasswordResetEmail: jasmine.Spy<jasmine.Func>;
    signOut: jasmine.Spy<jasmine.Func>;
  };
  let mockAngularFirestore: { collection: jasmine.Spy<jasmine.Func> };
  let mockMatSnackBar: { open: jasmine.Spy<jasmine.Func> };
  let mockRouter: { navigate: jasmine.Spy<jasmine.Func> };
  const pilot: IPilot = {
    email: 'John.Doe@email.com',
    firstName: 'John',
    lastName: 'Doe',
    issuingCountry: 'USA',
    licenseNumber: 'LN12345'
  };

  beforeEach(() => {
    mockAngularFireAuth = {
      createUserWithEmailAndPassword: jasmine.createSpy(
        'createUserWithEmailAndPassword'
      ),
      signInWithEmailAndPassword: jasmine
        .createSpy()
        .and.returnValue(Promise.resolve({})),
      sendPasswordResetEmail: jasmine
        .createSpy()
        .and.returnValue(Promise.resolve({})),
      signOut: jasmine.createSpy()
    };

    mockAngularFirestore = {
      collection: jasmine.createSpy('collection').and.returnValue({
        doc: jasmine.createSpy('doc').and.returnValue({
          set: jasmine.createSpy('set').and.returnValue(Promise.resolve()),
          update: jasmine.createSpy('update').and.returnValue(Promise.resolve()) // Add this line for update
        })
      })
    };

    mockMatSnackBar = {
      open: jasmine.createSpy('open')
    };

    mockRouter = {
      navigate: jasmine.createSpy('navigate')
    };

    TestBed.configureTestingModule({
      providers: [
        AuthService,
        { provide: AngularFireAuth, useValue: mockAngularFireAuth },
        { provide: AngularFirestore, useValue: mockAngularFirestore },
        { provide: MatSnackBar, useValue: mockMatSnackBar },
        { provide: Router, useValue: mockRouter }
      ]
    });

    service = TestBed.inject(AuthService);
  });

  it('should create service', () => {
    expect(service).toBeTruthy();
  });

  describe('register', () => {
    it('should create a new user and store additional information', async () => {
      const mockCredentials = { user: { uid: '12345' } };
      mockAngularFireAuth.createUserWithEmailAndPassword.and.returnValue(
        Promise.resolve(mockCredentials)
      );

      await service.register(pilot, 'password123');

      expect(
        mockAngularFireAuth.createUserWithEmailAndPassword
      ).toHaveBeenCalledWith(pilot.email, 'password123');
      expect(mockAngularFirestore.collection).toHaveBeenCalledWith('pilots');
      expect(mockAngularFirestore.collection().doc).toHaveBeenCalledWith(
        '12345'
      );
      expect(mockAngularFirestore.collection().doc().set).toHaveBeenCalledWith(
        pilot
      );
    });

    it('should throw an error if user creation fails', async () => {
      mockAngularFireAuth.createUserWithEmailAndPassword.and.returnValue(
        Promise.reject(new Error('User creation failed'))
      );

      try {
        await service.register(pilot, 'password123');
        fail('The test should have thrown an error');
      } catch (e) {
        const error = e as Error;
        expect(error.message).toBe('User creation failed');
      }
    });

    it('Snackbar should be triggerd when register occurs', async () => {
      const mockCredentials = { user: { uid: '12345' } };
      mockAngularFireAuth.createUserWithEmailAndPassword.and.returnValue(
        Promise.resolve(mockCredentials)
      );

      await service.register(pilot, 'password123');
      expect(mockMatSnackBar.open).toHaveBeenCalled();
    });
  });

  describe('login', () => {
    it('should call signInWithEmailAndPassword', async () => {
      const email = 'test@example.com';
      const password = 'password';

      await service.login(email, password);

      expect(
        mockAngularFireAuth.signInWithEmailAndPassword
      ).toHaveBeenCalledWith(email, password);
    });

    it('should call sendPasswordResetEmail', async () => {
      const email = 'test@example.com';

      await service.resetPassword(email);

      expect(mockAngularFireAuth.sendPasswordResetEmail).toHaveBeenCalledWith(
        email
      );
    });
  });
  describe('logout', () => {
    it('should call signOut and navigate to login page', async () => {
      await service.logOut();
      expect(mockAngularFireAuth.signOut).toHaveBeenCalled();
      expect(mockRouter.navigate).toHaveBeenCalledWith(['login']);
    });
  });

  describe('updateUser', () => {
    it('should update user profile successfully', async () => {
      const userDataUpdate: Partial<IPilot> = {
        firstName: 'UpdatedFirstName'
      };

      const mockUserId = 'mockUserId';
      spyOn(service, 'getCurrentUserID').and.returnValue(of(mockUserId));

      await service.updateUser(userDataUpdate);

      expect(mockAngularFirestore.collection).toHaveBeenCalledWith('pilots');
      expect(mockAngularFirestore.collection().doc).toHaveBeenCalledWith(
        mockUserId
      );
      expect(
        mockAngularFirestore.collection().doc().update
      ).toHaveBeenCalledWith(userDataUpdate);
      expect(mockMatSnackBar.open).toHaveBeenCalledWith(
        'Profile Updated Successfully',
        '',
        {
          duration: 5000
        }
      );
    });

    it('should handle failure to update user profile', async () => {
      const userDataUpdate: Partial<IPilot> = {
        firstName: 'UpdatedFirstName'
      };

      const mockError = new Error('Update failed');
      spyOn(service, 'getCurrentUserID').and.returnValue(of('mockUserId'));
      mockAngularFirestore
        .collection()
        .doc()
        .update.and.returnValue(Promise.reject(mockError));

      try {
        await service.updateUser(userDataUpdate);
        fail('The test should have thrown an error');
      } catch (error) {
        expect(error).toEqual(mockError);
        expect(mockMatSnackBar.open).toHaveBeenCalledWith(
          'Profile Update Failed',
          '',
          {
            duration: 5000
          }
        );
      }
    });

    it('should throw an error if user ID is not available', async () => {
      spyOn(service, 'getCurrentUserID').and.returnValue(of(null));

      try {
        await service.updateUser({});
        fail('The test should have thrown an error');
      } catch (error: any) {
        expect(error.message).toBe('User ID not available');
      }
    });
  });
});
