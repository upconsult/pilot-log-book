import { LogService } from './log.service';
import { HttpClient } from '@angular/common/http';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { IFlightLog } from 'src/app/models/flight-log.model';
import { of } from 'rxjs';

describe('LogService', () => {
  it('should return true after adding flight log', async () => {
    const firestoreAngularMock = {
      collection: jasmine.createSpy('collection').and.returnValue({
        doc: jasmine.createSpy('doc').and.returnValue({
          collection: jasmine.createSpy('collection').and.returnValue({
            add: jasmine.createSpy('add').and.returnValue(Promise.resolve({}))
          })
        })
      }),
      firestore: {},
      persistenceEnabled$: of(true),
      collectionGroup: jasmine.createSpy('collectionGroup').and.returnValue({
        add: jasmine.createSpy('add').and.returnValue(Promise.resolve({}))
      }),
      createId: jasmine.createSpy('createId').and.returnValue('')
    } as unknown as AngularFirestore;

    const mockHttpClient = {} as HttpClient; // Replace with a real instance or mock

    // Create an instance of AddLogService
    const logService = new LogService(mockHttpClient, firestoreAngularMock);

    // Spy on the addFlightLog method
    spyOn(logService, 'addFlightLog').and.callThrough();

    // Your flight log data
    const flightLog: IFlightLog = {
      id: '123',
      flightNumber: 123,
      balloonGroup: 'Group A',
      balloonType: 'Hot Air Balloon',
      balloonRegistration: 'ABC123',
      pilotInCommand: 'John Doe',
      typeOfOperation: 'Sightseeing',
      selectedCountryTakeOff: 'USA',
      selectedMunicipalityTakeOff: 'City A',
      selectedCountryLanding: 'USA',
      selectedMunicipalityLanding: 'City B',
      departureDateTime: new Date('2023-11-17T08:00:00'),
      arrivalDateTime: new Date('2023-11-17T10:00:00'),
      extraCommentary: 'Smooth flight with good weather conditions.',
      instructorExaminerName: 'Jane Smith',
      amountOfLandings: 1,
      typeOfFlightOptions: 'Day Flight',
      pilotType: 'Private Pilot',
      flightType: 'VFR',
      verified: false
    };

    const userId = 'MxCNkRIx92ZKhLZ6cXliwEQ0AL83';
    // Call the function and await the result
    const result = await logService.addFlightLog(flightLog, userId);

    // Assert that the result is true
    expect(result).toBe(true);

    // Check if addFlightLog was called with the correct arguments
    expect(logService.addFlightLog).toHaveBeenCalledWith(flightLog, userId);
  });
});
