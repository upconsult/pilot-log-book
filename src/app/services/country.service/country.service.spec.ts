import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { CountryService } from './country.service';

describe('CountryService', () => {
  let service: CountryService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [CountryService]
    });

    service = TestBed.inject(CountryService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should retrieve countries from the API via GET', () => {
    const mockCountries = [
      { name: 'Country1', population: 1000000 },
      { name: 'Country2', population: 2000000 }
      // Add more mock data as needed
    ];

    service.getCountries().subscribe(countries => {
      expect(countries).toEqual(mockCountries);
    });

    const req = httpMock.expectOne(service['apiUrl']);
    expect(req.request.method).toBe('GET');
    req.flush(mockCountries);
  });

  it('should handle errors when retrieving countries', () => {
    const errorMessage = 'Error retrieving countries';

    service.getCountries().subscribe(
      () => fail('should have failed with the error message'),
      error => {
        expect(error.error.message).toEqual(errorMessage);
      }
    );

    const req = httpMock.expectOne(service['apiUrl']);
    req.error(new ErrorEvent('error', { message: errorMessage }));
  });
});
