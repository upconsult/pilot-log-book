import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule, FormGroup, FormControl } from '@angular/forms';
import { AddFlightBalloondetailsComponent } from './add-flight-balloondetails.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('AddFlightBalloondetailsComponent', () => {
  let component: AddFlightBalloondetailsComponent;
  let fixture: ComponentFixture<AddFlightBalloondetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AddFlightBalloondetailsComponent],
      imports: [
        ReactiveFormsModule,
        MatFormFieldModule,
        MatAutocompleteModule,
        MatInputModule,
        BrowserAnimationsModule
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddFlightBalloondetailsComponent);
    component = fixture.componentInstance;

    component.flightForm = new FormGroup({
      balloonGroup: new FormControl('Gas'),
      balloonType: new FormControl('Type A'),
      balloonRegistration: new FormControl('ABC123')
    });

    fixture.detectChanges();
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it('should have a FormGroup input property', () => {
    expect(component.flightForm).toBeDefined();
  });
});
