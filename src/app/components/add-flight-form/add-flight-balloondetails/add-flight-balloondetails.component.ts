import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-add-flight-balloondetails',
  templateUrl: './add-flight-balloondetails.component.html',
  styleUrls: ['./add-flight-balloondetails.component.css']
})
export class AddFlightBalloondetailsComponent {
  @Input() flightForm!: FormGroup;
  options = ['opgeslagen in pilot model?'];
}
