import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule, FormControl, FormGroup } from '@angular/forms';
import { AddFlightLocationComponent } from './add-flight-location.component';
import { Observable, of } from 'rxjs';
import { CountryService } from 'src/app/services/country.service/country.service';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { AuthService } from 'src/app/services/auth.service';
import { IPilot } from 'src/app/models/pilot';

describe('AddFlightLocationComponent', () => {
  let component: AddFlightLocationComponent;
  let fixture: ComponentFixture<AddFlightLocationComponent>;
  let authService: jasmine.SpyObj<AuthService>;

  const mockUser: IPilot = {
    firstName: 'John',
    lastName: 'Doe',
    email: 'johndoe@gmail.com',
    issuingCountry: 'Belgium',
    licenseNumber: '123ABC'
  };

  const countryServiceStub = {
    getCountries: (): Observable<
      { name: { common: string }; region: string }[]
    > => {
      return of([
        { name: { common: 'Country A' }, region: 'Europe' },
        { name: { common: 'Country B' }, region: 'Europe' }
      ]);
    }
  };

  beforeEach(async () => {
    authService = jasmine.createSpyObj('AuthService', ['GetUserFromFirestore']);
    authService.GetUserFromFirestore.and.returnValue(of(mockUser));

    await TestBed.configureTestingModule({
      declarations: [AddFlightLocationComponent],
      imports: [
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatDatepickerModule,
        MatNativeDateModule,
        BrowserAnimationsModule,
        MatToolbarModule
      ],
      providers: [
        { provide: AuthService, useValue: authService },
        { provide: CountryService, useValue: countryServiceStub }
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddFlightLocationComponent);
    component = fixture.componentInstance;

    component.flightForm1 = new FormGroup({
      selectedCountryTakeOff: new FormControl('Belgium'),
      selectedMunicipalityTakeOff: new FormControl('Oudsbergen'),
      selectedCountryLanding: new FormControl('Belgium'),
      selectedMunicipalityLanding: new FormControl('Hasselt'),
      departureDateTime: new FormControl('01/01/2024'),
      arrivalDateTime: new FormControl('01/01/2024'),
      departureTime: new FormControl('12:00'),
      arrivalTime: new FormControl('12:00')
    });

    fixture.detectChanges();
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize countries$', () => {
    component.ngOnInit();
    component.countries$.subscribe(countries => {
      expect(countries.length).toBe(2);
      expect(countries).toEqual(['Country A', 'Country B']);
    });
  });
});
