import { ComponentFixture, TestBed } from '@angular/core/testing';
import {
  FormControl,
  FormGroup,
  FormsModule,
  ReactiveFormsModule
} from '@angular/forms';
import { AddFlightCommentaryComponent } from './add-flight-commentary.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('AddFlightCommentaryComponent', () => {
  let component: AddFlightCommentaryComponent;
  let fixture: ComponentFixture<AddFlightCommentaryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AddFlightCommentaryComponent],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        BrowserAnimationsModule
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddFlightCommentaryComponent);
    component = fixture.componentInstance;
    component.flightForm = new FormGroup({
      extraCommentary: new FormControl('commentary')
    });

    fixture.detectChanges();
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it('should have a FormGroup input property', () => {
    expect(component.flightForm).toBeDefined();
  });
});
