import {
  ComponentFixture,
  TestBed,
  async,
  fakeAsync,
  tick
} from '@angular/core/testing';
import { ReactiveFormsModule, FormControl, FormGroup } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { AddFlightFlightdetailsComponent } from './add-flight-flightdetails.component';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AuthService } from 'src/app/services/auth.service';
import { IPilot } from 'src/app/models/pilot';
import { of } from 'rxjs';

describe('AddFlightFlightdetailsComponent', () => {
  let component: AddFlightFlightdetailsComponent;
  let fixture: ComponentFixture<AddFlightFlightdetailsComponent>;
  let authService: jasmine.SpyObj<AuthService>;

  const mockUser: IPilot = {
    firstName: 'John',
    lastName: 'Doe',
    email: 'johndoe@gmail.com',
    issuingCountry: 'Belgium',
    licenseNumber: '123ABC'
  };

  beforeEach(async(() => {
    authService = jasmine.createSpyObj('AuthService', ['GetUserFromFirestore']);
    authService.GetUserFromFirestore.and.returnValue(of(mockUser));

    TestBed.configureTestingModule({
      declarations: [AddFlightFlightdetailsComponent],
      imports: [
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatRadioModule,
        MatSelectModule,
        BrowserAnimationsModule
      ],
      providers: [{ provide: AuthService, useValue: authService }]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddFlightFlightdetailsComponent);
    component = fixture.componentInstance;

    component.flightForm = new FormGroup({
      amountOfLandings: new FormControl(1),
      typeOfOperation: new FormControl('Sample operation'),
      pilotInCommand: new FormControl('Sample pilot'),
      pilotType: new FormControl('Flight instructor'),
      instructorExaminerName: new FormControl('Sample instructor')
    });

    // Trigger ngOnInit
    fixture.detectChanges();
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it('should have a FormGroup input property', fakeAsync(() => {
    // Trigger ngOnInit
    fixture.detectChanges();

    // Wait for asynchronous operations to complete
    tick();

    expect(component.flightForm).toBeDefined();
  }));
});
