import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule, FormGroup } from '@angular/forms';
import { FlightLogAddComponent } from './flight-log-add.component';
import { CountryService } from '../../../services/country.service/country.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AngularFireModule } from '@angular/fire/compat';
import { AngularFirestoreModule } from '@angular/fire/compat/firestore';
import { environment } from 'src/environments/environment';
import { AddFlightLocationComponent } from '../add-flight-location/add-flight-location.component';
import { AddFlightBalloondetailsComponent } from '../add-flight-balloondetails/add-flight-balloondetails.component';
import { AddFlightCommentaryComponent } from '../add-flight-commentary/add-flight-commentary.component';
import { AddFlightFlightdetailsComponent } from '../add-flight-flightdetails/add-flight-flightdetails.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatInputModule } from '@angular/material/input';
import { LogService } from '../../../services/log.service/log.service';
import { IFlightLog } from 'src/app/models/flight-log.model';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { of } from 'rxjs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { RouterTestingModule } from '@angular/router/testing';

describe('FlightLogAddComponent', () => {
  let component: FlightLogAddComponent;
  let fixture: ComponentFixture<FlightLogAddComponent>;
  let logService: LogService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        FlightLogAddComponent,
        AddFlightLocationComponent,
        AddFlightBalloondetailsComponent,
        AddFlightCommentaryComponent,
        AddFlightFlightdetailsComponent
      ],
      imports: [
        ReactiveFormsModule,
        HttpClientTestingModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFirestoreModule,
        MatFormFieldModule,
        BrowserAnimationsModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatInputModule,
        MatSnackBarModule,
        MatIconModule,
        MatToolbarModule,
        RouterTestingModule
      ],
      providers: [CountryService, LogService]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FlightLogAddComponent);
    component = fixture.componentInstance;
    logService = TestBed.inject(LogService);
    const dummyFlightLog: IFlightLog = {
      id: '123',
      flightNumber: 123,
      balloonGroup: 'Group A',
      balloonType: 'Type X',
      balloonRegistration: 'ABC123',
      pilotInCommand: 'John Doe',
      typeOfOperation: 'Operation Type',
      selectedCountryTakeOff: 'Country A',
      selectedMunicipalityTakeOff: 'Municipality A',
      selectedCountryLanding: 'Country B',
      selectedMunicipalityLanding: 'Municipality B',
      departureDateTime: new Date('2023-11-17T12:00:00Z'),
      arrivalDateTime: new Date('2023-11-17T12:00:00Z'),
      extraCommentary: 'Additional comments',
      instructorExaminerName: 'Instructor/Examiner Name',
      amountOfLandings: 3,
      typeOfFlightOptions: 'Flight options',
      pilotType: 'Pilot type',
      flightType: 'Flight type',
      verified: false
    };
    component.flightLog = dummyFlightLog;
    fixture.detectChanges();
  });

  it('should create the flightForms', () => {
    //flightform1
    expect(component.flightForm1).toBeTruthy();
    expect(component.flightForm1 instanceof FormGroup).toBeTruthy();
    //flightform2
    expect(component.flightForm2).toBeTruthy();
    expect(component.flightForm2 instanceof FormGroup).toBeTruthy();
    //flightform3
    expect(component.flightForm3).toBeTruthy();
    expect(component.flightForm3 instanceof FormGroup).toBeTruthy();
    //flightform4
    expect(component.flightForm4).toBeTruthy();
    expect(component.flightForm4 instanceof FormGroup).toBeTruthy();
  });

  it('should increment formIndex on nextPage', () => {
    //Create dummy data
    component.flightForm1.get('selectedCountryTakeOff')?.setValue('Belgium');
    component.flightForm1
      .get('selectedMunicipalityTakeOff')
      ?.setValue('Oudbergen');
    component.flightForm1
      .get('selectedMunicipalityLanding')
      ?.setValue('Belgium');
    component.flightForm1
      .get('departureDateTime')
      ?.setValue(new Date('2024-01-01T12:00:00Z'));
    component.flightForm1
      .get('arrivalDateTime')
      ?.setValue(new Date('2024-01-01T12:00:00Z'));
    component.flightForm1.get('departureTime')?.setValue('12:00');
    component.flightForm1.get('arrivalTime')?.setValue('12:00');
    //Set formIndex to 0
    component.formIndex = 0;
    //Call nextPage
    component.nextPage();
    //Expect formIndex to be 1
    expect(component.formIndex).toBe(1);
  });

  it('should decrement formIndex on previousPage', () => {
    component.formIndex = 2;
    component.previousPage();
    expect(component.formIndex).toBe(1);
  });

  it('should submit the form and navigate to logbook', () => {
    spyOn(component['router'], 'navigate');
    const addLogServiceSpy = spyOn(logService, 'addFlightLog').and.returnValue(
      Promise.resolve(true)
    );
    const userId = 'MxCNkRIx92ZKhLZ6cXliwEQ0AL83';

    spyOn(component['authService'], 'getCurrentUserID').and.returnValue(
      of(userId)
    );

    component.onSubmit();

    expect(addLogServiceSpy).toHaveBeenCalledWith(component.flightLog, userId);

    expect(component['router'].navigate).toHaveBeenCalledWith(['/logbook']);
  });

  it('should initialize the flightForm with empty fields', () => {
    const expectedForm1Values = {
      selectedCountryTakeOff: '',
      selectedMunicipalityTakeOff: '',
      selectedCountryLanding: '',
      selectedMunicipalityLanding: '',
      departureDateTime: '',
      departureTime: '12:00',
      arrivalDateTime: '',
      arrivalTime: '12:00'
    };

    const expectedForm2Values = {
      balloonRegistration: '',
      balloonGroup: '',
      balloonType: ''
    };

    const expectedForm3Values = {
      typeOfOperation: '',
      pilotInCommand: '',
      amountOfLandings: '1',
      pilotType: '',
      instructorExaminerName: ''
    };

    const expectedForm4Values = {
      extraCommentary: '',
      verified: false
    };

    expect(component.flightForm1.value).toEqual(expectedForm1Values);
    expect(component.flightForm2.value).toEqual(expectedForm2Values);
    expect(component.flightForm3.value).toEqual(expectedForm3Values);
    expect(component.flightForm4.value).toEqual(expectedForm4Values);
  });

  it('should mark the form as valid if required fields are filled', () => {
    component.flightForm1.get('selectedCountryTakeOff')?.setValue('Belgium');
    component.flightForm1
      .get('selectedMunicipalityTakeOff')
      ?.setValue('Oudbergen');
    component.flightForm1
      .get('selectedMunicipalityLanding')
      ?.setValue('Belgium');
    component.flightForm1
      .get('departureDateTime')
      ?.setValue(new Date('2024-01-01T12:00:00Z'));
    component.flightForm1
      .get('arrivalDateTime')
      ?.setValue(new Date('2024-01-01T12:00:00Z'));
    component.flightForm1.get('departureTime')?.setValue('12:00');
    component.flightForm1.get('arrivalTime')?.setValue('12:00');
    component.flightForm2.get('balloonRegistration')?.setValue('1');
    component.flightForm2.get('balloonGroup')?.setValue('Gas');
    component.flightForm2.get('balloonType')?.setValue('A');
    component.flightForm3.get('amountOfLandings')?.setValue('1');
    component.flightForm3.get('typeOfOperation')?.setValue('Normal');
    component.flightForm3.get('pilotInCommand')?.setValue('John');
    component.flightForm3.get('pilotType')?.setValue('Flight Instructor');
    component.flightForm3.get('instructorExaminerName')?.setValue('Doe');
    component.flightForm4.get('extraCommentary')?.setValue('Commentary');

    expect(component.flightForm1.valid).toBeTruthy();
    expect(component.flightForm2.valid).toBeTruthy();
    expect(component.flightForm3.valid).toBeTruthy();
    expect(component.flightForm4.valid).toBeTruthy();
  });
});
