import { TestBed, ComponentFixture } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { ProfileComponent } from './profile.component';
import { AuthService } from 'src/app/services/auth.service';
import { BehaviorSubject, Subscription } from 'rxjs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatFormFieldModule } from '@angular/material/form-field';
import { IPilot } from 'src/app/models/pilot';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('ProfileComponent', () => {
  let component: ProfileComponent;
  let fixture: ComponentFixture<ProfileComponent>;
  let authService: jasmine.SpyObj<AuthService>;
  let authServiceMock: { cachedUser$: BehaviorSubject<IPilot | null> };

  beforeEach(async () => {
    authServiceMock = {
      cachedUser$: new BehaviorSubject<IPilot | null>(null)
    };

    const authServiceSpy = jasmine.createSpyObj('AuthService', [
      'GetUserFromFirestore',
      'updateUser'
    ]);
    authServiceSpy.cachedUser$ = authServiceMock.cachedUser$;

    await TestBed.configureTestingModule({
      declarations: [ProfileComponent],
      imports: [
        ReactiveFormsModule,
        MatFormFieldModule,
        MatToolbarModule,
        MatInputModule,
        BrowserAnimationsModule
      ],
      providers: [{ provide: AuthService, useValue: authServiceSpy }]
    }).compileComponents();

    authService = TestBed.inject(AuthService) as jasmine.SpyObj<AuthService>;
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize profileForm with required fields', () => {
    expect(component.profileForm).toBeDefined();
    expect(component.profileForm.controls['licenseNumber']).toBeDefined();
    expect(component.profileForm.controls['issuingCountry']).toBeDefined();
    expect(component.profileForm.controls['firstName']).toBeDefined();
    expect(component.profileForm.controls['lastName']).toBeDefined();
    expect(component.profileForm.controls['licenseNumber'].valid).toBeFalsy();
    expect(component.profileForm.controls['issuingCountry'].valid).toBeFalsy();
    expect(component.profileForm.controls['firstName'].valid).toBeFalsy();
    expect(component.profileForm.controls['lastName'].valid).toBeFalsy();
  });
  /*
  it('should subscribe to userDataSubscription and update user data', () => {
    const mockUserData = {
      licenseNumber: 'ABCD1234',
      issuingCountry: 'Country',
      firstName: 'John',
      lastName: 'Doe'
    };
    const mockUser = {
      ...mockUserData,
      email:'JohnDoe@email.com'
    };
  
    // Mocking the behavior of GetUserFromFirestore method to return an Observable
    authService.GetUserFromFirestore.and.returnValue(of(mockUser));
  
    component.ngOnInit();
    expect(authService.GetUserFromFirestore).toHaveBeenCalled();
    expect(component.user).toEqual(mockUser);
    expect(component.profileForm.value).toEqual(mockUserData);
  });
*/
  it('should unsubscribe userDataSubscription on component destroy', () => {
    const unsubscribeSpy = jasmine.createSpy('unsubscribe');
    const subscription = new Subscription();
    subscription.unsubscribe = unsubscribeSpy as any; // Casting as any to avoid TypeScript error

    component.userDataSubscription = subscription;

    component.ngOnDestroy();
    expect(unsubscribeSpy).toHaveBeenCalled();
  });
});
