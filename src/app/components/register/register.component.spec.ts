import { ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';

import { RegisterComponent } from './register.component';
import { AuthService } from 'src/app/services/auth.service';

import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClient, HttpHandler } from '@angular/common/http';

describe('RegisterComponent', () => {
  let component: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;
  let mockAuthService: jasmine.SpyObj<AuthService>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RegisterComponent],
      imports: [
        MatFormFieldModule,
        ReactiveFormsModule,
        MatInputModule,
        BrowserAnimationsModule
      ],
      providers: [
        FormBuilder,
        { provide: AuthService, useValue: mockAuthService },
        HttpClient,
        HttpHandler
      ]
    }).compileComponents();
    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  //initialization tests
  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('form should be invalid on init', () => {
    expect(component.registerForm.valid).toBeFalsy();
  });

  it('form should be valid when filled out correctly', () => {
    fakeAsync(() => {
      component.registerForm.setValue({
        firstName: 'John',
        lastName: 'Doe',
        email: 'john.doe@example.com',
        licenseNumber: '123456',
        issuingCountry: 'Belgium',
        password: 'password123',
        confirmPassword: 'password123'
      });
      expect(component.registerForm.valid).toBeTruthy();
    });
  });
});
