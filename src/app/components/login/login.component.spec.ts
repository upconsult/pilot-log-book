import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { AuthService } from 'src/app/services/auth.service';

import { FormBuilder } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field'; // Import MatFormFieldModule
import { MatInputModule } from '@angular/material/input'; // Import MatInputModule
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; // Import BrowserAnimationsModule
import { PasswordResetDialogueComponent } from './password-reset-dialogue/password-reset-dialogue.component';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let authService: jasmine.SpyObj<AuthService>;
  let dialog: jasmine.SpyObj<MatDialog>;

  beforeEach(() => {
    authService = jasmine.createSpyObj('AuthService', ['login']);
    dialog = jasmine.createSpyObj('MatDialog', ['open']);

    TestBed.configureTestingModule({
      declarations: [LoginComponent],
      imports: [
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        BrowserAnimationsModule
      ], // Add Material modules
      providers: [
        FormBuilder,
        { provide: AuthService, useValue: authService },
        { provide: MatDialog, useValue: dialog }
      ]
    });

    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create LoginComponent', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize loginForm with empty values', () => {
    expect(component.loginForm.get('email')?.value).toBe('');
    expect(component.loginForm.get('password')?.value).toBe('');
  });

  it('form should be invalid on init', () => {
    expect(component.loginForm.valid).toBeFalsy();
  });

  it('form should be valid when filled out correctly', () => {
    component.loginForm.setValue({
      email: 'test.test@hotmail.com',
      password: 'password123'
    });
    expect(component.loginForm.valid).toBeTruthy();
  });

  it('should call loginUser method', () => {
    component.loginForm.setValue({
      email: 'test@example.com',
      password: 'password123'
    });

    component.loginUser();
    expect(authService.login).toHaveBeenCalledWith(
      'test@example.com',
      'password123'
    );
  });

  it('should open password reset dialog', () => {
    component.openDialog();
    expect(dialog.open).toHaveBeenCalledWith(PasswordResetDialogueComponent);
  });
});
