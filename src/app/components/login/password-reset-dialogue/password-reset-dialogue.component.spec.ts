import {
  ComponentFixture,
  TestBed,
  fakeAsync,
  tick
} from '@angular/core/testing';
import { of } from 'rxjs'; // Import 'of'
import { FormBuilder } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PasswordResetDialogueComponent } from './password-reset-dialogue.component';

// Import MatDialog and create a mock for it
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { AuthService } from 'src/app/services/auth.service';
class MatDialogMock {
  open() {
    return {
      afterClosed: () => of({})
    };
  }
}

describe('PasswordResetDialogueComponent', () => {
  let component: PasswordResetDialogueComponent;
  let fixture: ComponentFixture<PasswordResetDialogueComponent>;
  let authService: jasmine.SpyObj<AuthService>;

  beforeEach(() => {
    authService = jasmine.createSpyObj('AuthService', ['resetPassword']);

    TestBed.configureTestingModule({
      declarations: [PasswordResetDialogueComponent],
      imports: [
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        BrowserAnimationsModule,
        MatDialogModule
      ],
      providers: [
        FormBuilder,
        { provide: AuthService, useValue: authService },
        // Provide the mock MatDialog service
        { provide: MatDialog, useClass: MatDialogMock }
      ]
    });

    fixture = TestBed.createComponent(PasswordResetDialogueComponent);
    component = fixture.componentInstance;
  });

  it('should create PasswordResetDialogueComponent', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize resetPasswordForm with empty email', () => {
    expect(component.resetPasswordForm.get('email')?.value).toBe('');
  });

  it('form should be invalid with an empty email', () => {
    expect(component.resetPasswordForm.valid).toBeFalsy();
  });

  it('form should be valid with a non-empty email', () => {
    component.resetPasswordForm.setValue({ email: 'test@example.com' });
    expect(component.resetPasswordForm.valid).toBeTruthy();
  });

  it('should call forgotPassword method when submitting the form', fakeAsync(() => {
    const email = 'test@example.com';
    component.resetPasswordForm.setValue({ email });

    authService.resetPassword.and.returnValue(Promise.resolve());

    component.forgotPassword();
    tick(); // Simulate the passage of time for the async operation

    expect(authService.resetPassword).toHaveBeenCalledWith(email);
  }));

  it('should handle an error when calling forgotPassword method', fakeAsync(() => {
    const email = 'test@example.com';
    component.resetPasswordForm.setValue({ email });

    const error = new Error('Reset password failed');
    authService.resetPassword.and.returnValue(Promise.reject(error));

    spyOn(console, 'error');

    component.forgotPassword();
    tick(); // Simulate the passage of time for the async operation

    expect(authService.resetPassword).toHaveBeenCalledWith(email);
    expect(console.error).toHaveBeenCalledWith('Error during login: ', error);
  }));
});
