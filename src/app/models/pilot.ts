export interface IPilot {
  email: string;
  firstName: string;
  lastName: string;
  issuingCountry: string;
  licenseNumber: string;
}
