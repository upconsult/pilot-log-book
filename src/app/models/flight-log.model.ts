export interface IFlightLog {
  //Incrementeel startnummer wordt gedefineerd ij registreren
  flightNumber: number;
  balloonGroup: string;
  balloonType: string;
  //G
  balloonRegistration: string;
  //G
  pilotInCommand: string;
  //G
  typeOfOperation: string;
  selectedCountryTakeOff: string;
  //G
  selectedMunicipalityTakeOff: string;
  selectedCountryLanding: string;
  //G
  selectedMunicipalityLanding: string;
  //G
  departureDateTime: Date;
  //G
  arrivalDateTime: Date;
  extraCommentary: string;

  instructorExaminerName: string;
  amountOfLandings: number;
  typeOfFlightOptions: string;
  pilotType: string;
  //G
  flightType: string;
  //G
  verified: boolean;
  id: string;
}
