module.exports = {
  ci: {
    collect: {
      url: ['http://localhost:4200', 'http://localhost:4200/register'],
      maxWaitForLoad: 2000,
      settings: {
        emulatedFormFactor: 'mobile',
        chromeFlags: '--no-sandbox'
      },
      numberOfRuns: 1
    }
  }
};
