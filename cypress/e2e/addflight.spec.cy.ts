describe('Add Flight Page', () => {
  before(() => {
    cy.clearAllLocalStorage;
    cy.clearAllSessionStorage;
    cy.clearCookies;
    cy.login();
    cy.visit(Cypress.env('baseUrl') + '/addlog');
  });

  it('should navigate between form pages', () => {
    // Check navigating back to the first page
    cy.get('.back-button')
      .click({ force: true })
      .click({ force: true })
      .click({ force: true }); // Assuming three pages in between
  });

  // Function to wait for the button to be enabled and click it
  const waitForAndClickNextButton = () => {
    cy.get('.next-button').should('be.enabled').click();
  };
  // check if log can be created
  it('should fill in and submit form.', () => {
    // Fill in the first form
    cy.get('.form-container').within(() => {
      cy.get('[formControlName="selectedCountryTakeOff"]').select('Belgium', {
        force: true
      });
      cy.get('[formControlName="selectedMunicipalityTakeOff"]').type(
        'Oudsbergen',
        { force: true }
      );
      cy.get('[formControlName="selectedMunicipalityLanding"]').type('Diest', {
        force: true
      });
      cy.get('[formControlName="departureDateTime"]').type('01/01/2024', {
        force: true
      });
    });

    // Proceed to the next form
    cy.get('.next-button').should('be.visible').click({ force: true });

    // Fill in the second form
    cy.get('.form-container').within(() => {
      cy.get('[formControlName="balloonRegistration"]').type('1', {
        force: true
      });
      cy.get('[formControlName="balloonGroup"]').type('Gas', { force: true });
      cy.get('[formControlName="balloonType"]').type('A', { force: true });
    });

    // Proceed to the next form
    waitForAndClickNextButton();

    // Fill in the third form
    cy.get('.form-container').within(() => {
      cy.get('[formControlName="amountOfLandings"]').select('1', {
        force: true
      });
      cy.get('[formControlName="typeOfOperation"]').type('Normal', {
        force: true
      });
      cy.get('[formControlName="pilotInCommand"]').type('John', {
        force: true
      });
      cy.get('[formControlName="pilotType"]')
        .contains('Flight instructor')
        .click();
      cy.get('[formControlName="instructorExaminerName"]').type('Johnny', {
        force: true
      });
    });

    // Proceed to the next form
    cy.get('.next-button').should('be.visible').click({ force: true });

    // Fill in and submit the fourth form
    cy.get('.form-container').within(() => {
      cy.get('[formControlName="extraCommentary"]').type('commentary', {
        force: true
      });
    });

    // Click the submit button
    cy.get('.submit-button').should('be.visible').click({ force: true });
  });
});
