describe('Visit App Test', () => {
  afterEach(() => {
    cy.clearCookies();
    cy.clearLocalStorage();
  });

  it('Visits the initial page', () => {
    cy.visit(Cypress.env('baseUrl'));
  });
});
