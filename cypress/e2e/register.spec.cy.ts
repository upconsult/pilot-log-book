describe('Visit Register Test', () => {
  it('Visits the register page', () => {
    cy.visit(Cypress.env('baseUrl') + '/register');
  });
});

describe('Register Form Contents', () => {
  it('Register form is visible', () => {
    cy.visit(Cypress.env('baseUrl') + '/register');
  });

  it('Register form contents', () => {
    cy.get('.register-form').should('be.visible');
    cy.get('.register-form').contains('Register');
    cy.get('.register-form').contains('License Number');
    cy.get('.register-form').contains('Issuing Country');
    cy.get('.register-form').contains('First Name');
    cy.get('.register-form').contains('Last Name');
    cy.get('.register-form').contains('E-mail');
    cy.get('.register-form').contains('Password');
  });

  it('Register form submit button', () => {
    cy.get('.mdc-button__label').should('be.visible');
    cy.get('.mdc-button__label').contains('Register');
  });
});

describe('Register Test', () => {
  it('Register a new user', () => {
    cy.fixture('register-user').then(user => {
      cy.visit(Cypress.env('baseUrl') + '/register');
      cy.get('#mat-input-0').type(user.licenseNumber);
      cy.get('#mat-input-1').select(user.issuingCountry);
      cy.get('#mat-input-2').type(user.firstName);
      cy.get('#mat-input-3').type(user.lastName);
      cy.get('#mat-input-4').type(user.email);
      cy.get('#mat-input-5').type(user.password);
      cy.get('#mat-input-6').type(user.confirmPassword);
      cy.get('.register-form').submit();
      cy.wait(1000);
    });
  });
});

describe('Already Registered Test', () => {
  it('Already Registered button', () => {
    cy.get('.mat-mdc-button > .mdc-button__label').should('be.visible');
    cy.get('.mat-mdc-button > .mdc-button__label').contains(
      'Already have an account?'
    );
  });
});
