describe('Visit Login Test', () => {
  it('Visits the login page', () => {
    cy.visit(Cypress.env('baseUrl'));
  });
});

describe('Login Form Contents', () => {
  it('Login form is visible', () => {
    cy.visit(Cypress.env('baseUrl'));
  });

  it('Register button is visible', () => {
    cy.visit(Cypress.env('baseUrl'));
    cy.get('.mdc-button__label').should('contain', 'Forgot password?');
  });

  it('Password-reset button is visible', () => {
    cy.visit(Cypress.env('baseUrl'));
    cy.get('.mdc-button__label').should('contain', 'Register');
  });
});

describe('Email test - Wrong input', () => {
  it('Login with false mail', () => {
    cy.fixture('login-user').then(() => {
      cy.visit(Cypress.env('baseUrl'));
      cy.get('#mat-input-0').type('john');
      cy.get('#mat-input-1').click();
      cy.get('#mat-mdc-error-0').should(
        'contain',
        'Please enter a valid email!'
      );
    });
  });
});

describe('Login Test', () => {
  it('Login a user', () => {
    cy.fixture('login-user').then(user => {
      cy.visit(Cypress.env('baseUrl'));
      cy.get('#mat-input-0').type(user.email);
      cy.get('#mat-input-1').type(user.password);
      cy.get('.login-form').submit();
    });
  });
});

describe('Login button disabled with missing password', () => {
  it('Button is disabled when only email is entered', () => {
    cy.visit(Cypress.env('baseUrl'));
    cy.get('#mat-input-0').type('johndoe@gmail.com');
    cy.get('.mdc-button').should('be.disabled');
  });
});
