import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

export const verifyAllLogs = functions
  .region('europe-west1')
  .https.onCall(async data => {
    const flightNumber = data.flightNumber;
    functions.logger.info('verifyAllLogs invoked');
    // get all logs from firestore
    const logs = (await admin.firestore().collectionGroup('logs').get()).docs;
    functions.logger.info(`found ${logs.length} unverified logs`);
    const logUpdates = [];
    for (const log of logs) {
      if (log.data().flightNumber == flightNumber) {
        log.ref.set({ verified: true }, { merge: true });
      }
    }
    functions.logger.info(`verified ${logUpdates.length} logs`);
    return null;
  });
