import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

admin.initializeApp();

const firestore = admin.firestore();

export const addLogFunction = functions.firestore
  .document('pilots/{pilotId}/logs/{logId}')
  .onCreate(async (snapshot, context) => {
    const pilotId = context.params.pilotId;
    const logId = context.params.logId;

    // Get the pilot document reference
    const pilotDocRef = firestore.collection('pilots').doc(pilotId);

    // Get the log document
    const logDocRef = firestore
      .collection('pilots')
      .doc(pilotId)
      .collection('logs')
      .doc(logId);

    // Get the number of logs for the pilot
    const logCount =
      firestore
        .collection('pilots')
        .doc(pilotId)
        .collection('logs')
        .count()
        .get() || 0;

    // Get the flight log data
    const flightLogData = snapshot.data() as {
      departureDateTime: admin.firestore.Timestamp;
      arrivalDateTime: admin.firestore.Timestamp;
    };

    // Calculate the new flight number, totalflight time and flight time for log
    const newFlightNumber = (await logCount).data().count;
    const flightTime = calculateFlightTime(flightLogData);
    const totalFlightTime = (
      await calculateTotalFlightTime(pilotId, flightTime)
    ).valueOf();

    // Update the existing log document with flight time and new flight number
    await logDocRef.set(
      {
        flightTime,
        flightNumber: newFlightNumber,
        totalFlightTime
      },
      { merge: true }
    );

    // Update the pilot document with the new flight number
    await pilotDocRef.update({
      flightNumber: newFlightNumber
    });
    return null;
  });

function calculateFlightTime(flightLogData: {
  departureDateTime: admin.firestore.Timestamp;
  arrivalDateTime: admin.firestore.Timestamp;
}): number {
  // Convert departure and arrival dates to Date objects
  const departureTimestamp = flightLogData.departureDateTime.toDate();
  const arrivalTimestamp = flightLogData.arrivalDateTime.toDate();

  // Calculate flight time
  const flightDuration =
    arrivalTimestamp.getTime() - departureTimestamp.getTime();

  const minutes = Math.floor(flightDuration / 60000); // 1 minute = 60000 milliseconds
  return minutes;
}

async function calculateTotalFlightTime(
  pilotId: string,
  flightTime: number
): Promise<number> {
  try {
    // Get all logs for the specified pilot
    const logsSnapshot = await firestore
      .collection('pilots')
      .doc(pilotId)
      .collection('logs')
      .get();

    const logDocs = logsSnapshot.docs;

    if (logDocs.length >= 2) {
      logDocs.sort((a, b) => {
        const flightNumberA = a.data().flightNumber || 0;
        const flightNumberB = b.data().flightNumber || 0;

        return flightNumberA - flightNumberB;
      });
    }

    // Get the last log from the array
    const lastLogDoc = logDocs[logDocs.length - 1];

    // Get the total flight time from the last log
    let totalFlightTime = lastLogDoc.get('totalFlightTime') || 0;

    if (totalFlightTime != 0) {
      totalFlightTime += flightTime;
    } else {
      totalFlightTime = flightTime;
    }
    return totalFlightTime;
  } catch (error) {
    console.error('Error calculating total flight time:', error);
    return 0;
  }
}

export const deleteLogFunction = functions.firestore
  .document('pilots/{pilotId}/logs/{logId}')
  .onDelete(async (snapshot, context) => {
    const pilotId = context.params.pilotId;
    // Get the pilot document reference
    const pilotDocRef = firestore.collection('pilots').doc(pilotId);

    const logCount =
      firestore
        .collection('pilots')
        .doc(pilotId)
        .collection('logs')
        .count()
        .get() || 0;

    let newFlightNumber = (await logCount).data().count;

    if (newFlightNumber < 0) {
      newFlightNumber = 0;
    }

    await pilotDocRef.update({
      flightNumber: newFlightNumber
    });

    return null;
  });
