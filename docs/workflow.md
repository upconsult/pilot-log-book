# Gitflow Workflow

[⬅️ Go back](../README.md)

Gitflow is a popular branching model and workflow strategy for version control using Git. It provides a structured approach to managing code in software development projects, helping teams maintain a stable main branch while developing new features and handling releases and hotfixes.

## Key Branches

- **Main Branch:** Represents the stable, production-ready code.
- **Develop Branch:** Where ongoing development and feature integration occur.

## Supporting Branches

- **Feature Branches:** Created for developing new features.
- **Release Branches:** Used to prepare and test code for release.
- **Hotfix Branches:** For addressing critical issues in production code.
- **Merging Branches:** Temporary branches for resolving merge conflicts
- ...

## Workflow

1. Start a new feature in a feature branch.
2. Develop and test the feature in the feature branch.
3. Merge the feature branch into the develop branch.
4. Create a release branch to prepare for deployment.
5. Test and finalize the release branch, then merge it into both main and develop.
6. Address hotfixes by creating hotfix branches and merging them back into master and develop.

## Visualization

![gitflow.png](images/gitflow.png)
