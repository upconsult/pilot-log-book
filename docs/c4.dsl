workspace {

    model {
        pilotPerson = person "Pilot" "A person that flies air balloons." "Customer"
        fiPerson = person "Flight Instructor" "A person that can log special flights." "Customer"
        fePerson = person "Flight Examiner" "A person that can log special flights." "Customer"
        dglvPerson = person "DGLV (Federal Goverment)" "Governments that provides legal data." "Customer"

        
        fliteSoftwareSystem = softwareSystem "Flite" "Allows pilots to log flights, access logged flights and view EASA regulations." "Software System"{
            singlePageApplicationContainer = container "Single Page Application" "Provides all of the pilot logbook functionality. And is hosted on Firebase Hosting." "JavaScript and Angular" "Web Browser"
            
            authenticationContainer = container "Authentication" "Stores all accounts related information." "Firebase Authentication"
            DatabaseContainer = container "Database" "Stores pilots flight logs." "Firebase Firestore Database" "Database"
            CloudFunctionsContainer = container "Cloud Functions" "Provides backend functionality." "Firebase Cloud Functions"
            NotificationServiceContainer = container "Notification Service" "Sends users notifications when flights are needed." "Firebase Cloud Messaging"
        }
        
        entryVerificationSoftwareSystem = softwareSystem "Entry Verification System" "Future system that can check the validity of a flight." "Existing System"
        operatorSoftwareSystem = softwareSystem "Operator System" "Allows operators to manage flight data of air balloons." "Existing System"


        # relationships between people and software systems
        pilotPerson -> fliteSoftwareSystem "View / Record logged flights and consult EASA regulations"
        fiPerson -> fliteSoftwareSystem "View / Record logged flights and consult EASA regulations"
        fePerson -> fliteSoftwareSystem "View / Record logged flights and consult EASA regulations"
        fliteSoftwareSystem -> entryVerificationSoftwareSystem "Sends entries made in pilot logs and verifies them"
        
        entryVerificationSoftwareSystem -> dglvPerson "Gets legal data"
        operatorSoftwareSystem -> entryVerificationSoftwareSystem "Sends entries made in air balloon logs"

        
        # relationships to/from containers
        pilotPerson -> singlePageApplicationContainer "Visits flite.quickrelease.aero using [HTTPS]"
        fiPerson -> singlePageApplicationContainer "Visits flite.quickrelease.aero using [HTTPS]"
        fePerson -> singlePageApplicationContainer "Visits flite.quickrelease.aero using [HTTPS]"

        singlePageApplicationContainer -> authenticationContainer "Authenticates and registers users"
        singlePageApplicationContainer -> DatabaseContainer "Reads from and writes to [NOSQL/TCP]"

        CloudFunctionsContainer -> authenticationContainer "New users triggers functions"
        CloudFunctionsContainer -> DatabaseContainer "New entries triggers functions"
        CloudFunctionsContainer -> NotificationServiceContainer "Information on when notifications should be sent"
        CloudFunctionsContainer -> entryVerificationSoftwareSystem "Sends flight log verification requests"

        NotificationServiceContainer -> pilotPerson "Sends notifications to users"
        NotificationServiceContainer -> fiPerson "Sends notifications to users"
        NotificationServiceContainer -> fePerson "Sends notifications to users"
    }

    views {
        systemLandscape fliteSoftwareSystem "SystemLandscape" {
            include *
            autolayout lr
        }

        systemContext fliteSoftwareSystem "SystemContext"{
            include *
            autolayout
        }

        container fliteSoftwareSystem "Container" {
            include *
            autolayout
        }

        styles {
            element "Person" {
                color #ffffff
                fontSize 22
                shape Person
            }
            element "Bank Staff" {
                background #999999
            }
            element "Software System" {
                background #1168bd
                color #ffffff
            }
            element "Existing System" {
                background #999999
                color #ffffff
            }
            element "Container" {
                background #438dd5
                color #ffffff
            }
            element "Web Browser" {
                shape WebBrowser
            }
            element "Mobile App" {
                shape MobileDevicePortrait
            }
            element "Database" {
                shape Cylinder
            }
            element "Component" {
                background #85bbf0
                color #000000
            }
            element "Failover" {
                opacity 25
            }
        }
        
        theme default
    }
}
