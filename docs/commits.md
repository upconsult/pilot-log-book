# Commit Message Convention

[⬅️ Go back](../README.md)

For commit messages, we can combine and simplify the Angular [Commit Message Guideline](https://github.com/angular/angular/blob/22b96b9/CONTRIBUTING.md#-commit-message-guidelines) and the [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/#summary) guideline.

## Commit Linter

We use [commitlint](https://commitlint.js.org/#/) to lint our commit messages. This ensures that all commit messages follow the same format. The commit linter is configured in the [commitlint.config.js](../commitlint.config.js) file.

## Summary

### Category

The commit message should begin with a change category. Since we're following the flexible Conventional message guideline, you have the option to select from categories like `feat`, `fix`, `refactor`, `chore` or `...` any other type that suits your change.

Common types:

- `feat` is for adding a new feature
- `fix` is for fixing a bug
- `refactor` is for changing code for performance or convenience purpose (e.g. readability)
- `chore` is for maintenance tasks

#### Optional Scope

The scope adds additional context to the commit message, for example `feat(auth)` or `fix(search)`. With the flexibility we allow, this scope is entirely optional.

#### Description

Following the `category(type)`, include a colon "`:`" to introduce the commit description. The description should be written in the present tense, starting with a capital letter, and conclude with a semicolon "`;`" if there is a body present.

The commit body is optional when the commit diff involves minor changes. However, if the diff encompasses multiple changes, it's advisable to include a commit body. The body should contain a list of changes, with each change starting with a capital letter. You can structure the list using Markdown's `-` character for bullet points.

**!!! Make sure the message is in present tense !!!**

### Examples:

```markdown
feat: add new button component;

- Add new button component to view
- Add button style
```

```markdown
fix: add the stop directive to button component to prevent propagation
```

```markdown
refactor: rewrite button component in TypeScript;

- add dynamic style
- Change content of button
```

```markdown
chore: write button documentation
---or---
docs: write button documentation
```
