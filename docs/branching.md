## Summary

[⬅️ Go back](../README.md)

### Category

A git branch should start with a category. Pick one of these: `feature`, `release`, `test`, `bugfix`, `hotfix`, `experimental`, `merging` or `chore`.

- `feature` is for adding, refactoring or removing a feature
- `release` is for preparing and testing code for a new release
- `test` is for adding tests to existing feature code if this branch didn't add sufficient testing
- `bugfix` is for fixing a bug
- `hotfix` is for changing code with a temporary solution and/or without following the usual process (usually because of an emergency)
- `experimental` is for experimenting outside an issue/ticket
- `merging` is a temporary branch for resolving merge conflicts
- `chore` is for everything else (writing documentation, formatting, adding tests, cleaning useless code etc.)

### Description

After the category, there should be another "`/`" followed by a description which sums up the purpose of this specific branch. This description should be short and "kebab-cased".

By default, you can use the title of the issue/ticket you are working on. Just replace any special character by "`-`".

### To sum up, follow this pattern when branching

```plaintext
git branch <category/description-in-kebab-case>
```

**Examples:**

- You need to add, refactor or remove a feature: `git branch feature/create-new-button-component`
- You need to fix a bug: `git branch bugfix/button-overlap-form-on-mobile`
- You need to fix a bug really fast (possibly with a temporary solution): `git branch hotfix/registration-form-not-working`
- You want to experiment outside an issue/ticket: `git branch experimental/refactor-components-with-atomic-design`
